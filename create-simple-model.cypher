// tags
CREATE (feedback:Tag {name:'Feedback'})
CREATE (communication:Tag {name:'Communication'})
CREATE (learning:Tag {name:'Learning'})
CREATE (creativity:Tag {name:'Creativity'})
CREATE (idea:Tag {name:'Idea'})
// tag types
CREATE (topic:TagType {name:'Topic'})
// actors
CREATE (player:ActorType {name:'Mind Settlers Player'})
CREATE (author:ActorType {name:'Mind Settlers Author'})
// people
CREATE (mat:Person {name:'Mathias Daus'})
CREATE (jurgen:Person {name:'Jurgen Appelo'})
// content
CREATE (dailyLearning:Practice {name:'Daily Learning'})
CREATE (workLifeOptimizer:Track {name:'Daily Work-Life Optimizer'})
CREATE (d1:Practice {name:'Daily Planning'})
CREATE (d2:Practice {name:'Daily Collaboration'})
CREATE (d3:Practice {name:'Daily Networking'})
CREATE (d4:Practice {name:'Daily Reflection'})

// jurgen's relations
CREATE (jurgen)-[:ACTS_AS]->(player)
CREATE (jurgen)-[:ACTS_AS]->(author)
CREATE (jurgen)-[:AUTHOR_OF]->(dailyLearning)

// mat's relations
CREATE (mat)-[:ACTS_AS]->(player)
CREATE (mat)-[:USED_FOR_MARK {count: 2}]->(learning)
CREATE (mat)-[:USED_FOR_MARK {count: 1}]->(feedback)

// tag relations
CREATE (dailyLearning)-[:HAS_TAGS]->(learning)
CREATE (workLifeOptimizer)-[:HAS_PRACTICES]->(dailyLearning)
CREATE (workLifeOptimizer)-[:HAS_PRACTICES]->(d1)
CREATE (workLifeOptimizer)-[:HAS_PRACTICES]->(d2)
CREATE (workLifeOptimizer)-[:HAS_PRACTICES]->(d3)
CREATE (workLifeOptimizer)-[:HAS_PRACTICES]->(d4)

CREATE (learning)-[:RELATED_TO]->(feedback)
CREATE (learning)-[:SIBLING_OF]->(creativity)
CREATE (learning)-[:HAS_PARENT]->(idea)
CREATE (feedback)-[:RELATED_TO]->(communication)

CREATE (feedback)-[:TYPE_OF]->(topic)
CREATE (communication)-[:TYPE_OF]->(topic)
CREATE (learning)-[:TYPE_OF]->(topic)
CREATE (creativity)-[:TYPE_OF]->(topic)
CREATE (idea)-[:TYPE_OF]->(topic)

