
## Installation

- set path to wher your scripts are:

```bash
export PATH=$PATH:/Users/mat/Library/Application\ Support/Neo4j\ Desktop/Application/neo4jDatabases/database-90af7347-fed9-4611-bbb7-9c24f04d7e66/installation-3.4.4/bin
```
- patch your script to make them match your Java version
``` bash
$> java -v 
java version "10.0.1" 2018-04-17
```

```bash
$> sh ./neo4j-import               
Unable to find any JVMs matching version "1.8.3".
```
- `vi neo4j-import` and find the version number and replace it by your version number

```bash
_find_java_home() {
  [[ "${JAVA_HOME:-}" ]] && return

  case "${DIST_OS}" in
    "macosx")
      JAVA_HOME="$(/usr/libexec/java_home -v 10.0.1)"
      ;;
    "gentoo")
      JAVA_HOME="$(java-config --jre-home)"
      ;;
  esac
}
```
# Import

- delete the old graph database
- import nodes and header csvs

```bash
$> rm -rf /Users/mat/Library/Application\ Support/Neo4j\ Desktop/Application/neo4jDatabases/database-90af7347-fed9-4611-bbb7-9c24f04d7e66/installation-3.4.4/data/databases/graph.db \
&& neo4j-admin import --delimiter "," --nodes "data/tag-nodes-header.csv,data/tag-nodes.csv" --nodes "data/practice-nodes-header.csv,data/practice-nodes.csv" --relationships:HAS "data/practice-tag-relation-header.csv,data/practice-tag-relation.csv"
```
- attention: when importing the nodes, the header-csv have to be mentioned first. Inspect the command above.
- for convinience and better maintenance, the import script can be found in `package.jsom` 

```bash
npm run build
```

## restart node server

- set neo4j installation path

```bash
$>echo 'NEO4J=<path-to-your-installation>/neo4jDatabases/database-90af7347-fed9-4611-bbb7-9c24f04d7e66/installation-3.4.4/bin' >> ~/.bash_profile
$>echo 'PATH=$PATH:$NEO4J' >> ~/.bash_profile
$> . ~/.bash_profile
```
- restart server

```bash
$> ./neo4j restart
```

- open browser on `http://localhost:7474/`
- login with
  - user: neo4j
  - pass: 00


## Importing Resources
- [tips for first import] (https://github.com/neo4j-examples/neo4j-movies-template/blob/master/README.md)
- [basic example] (https://neo4j.com/docs/operations-manual/current/tutorial/import-tool/)


# Queries

```cypher
MATCH (a:Author) WHERE a.name = 'Jurgen Appelo' RETURN a
```

* [basic queries]( https://www.remwebdevelopment.com/blog/sql/some-basic-and-useful-cypher-queries-for-neo4j-201.html
)

find tags that have more than 20 practices
```cypher
START n=node(*)
MATCH (n:Tag)-[r]-(:Practice)
WITH n, COUNT(r) AS rel_count
WHERE rel_count > 20
RETURN n
```